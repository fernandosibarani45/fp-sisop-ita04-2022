#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>

#define PORT 8080
#define LENGTH 1024
#define PERMISSION_FILE "permission.dat"
#define USER_DATA_FILE "user.dat"
#define DATABASES_FOLDER "database/databases"
#define LOG_FILE "logUser.log"

char buffer[LENGTH] = {0};
char username[LENGTH] = "", databaseUsed[100] = "";
int root = 0;

struct allowed{
	char name[LENGTH];
	char password[LENGTH];
};

struct databasePermission{
	char name[LENGTH];
	char database[LENGTH];
};

typedef struct column {
    char nama[LENGTH];
    char type[8];
} column;

char *sendMessage(int socket, char* message){
	int temp;
    bzero(buffer, sizeof(buffer));
	temp = send(socket , message , sizeof(buffer) , 0 );
	if(!temp){
		printf("Terdapat kesalahan dalam pengiriman pesan");
		exit(EXIT_FAILURE);
	}
}

char *recvMessage(int socket){
	int temp;
    bzero(buffer, sizeof(buffer));
	temp = recv(socket , buffer, LENGTH, 0);
	if(!temp){
		printf("Terdapat kesalahan dalam penerimaan pesan");
		exit(EXIT_FAILURE);
	}

    return buffer;
}

int cekRoot(int socket){
    int res;
    res = atoi(recvMessage(socket));
    if(!res){
        strcpy(username, "ROOT");
        return 1;
    }
    return 0;
}

int auth(int socket){
    FILE *fp;
	struct allowed user;
	int found=0;
    char userName[LENGTH], passWord[LENGTH];
    strcpy(userName, recvMessage(socket));
    strcpy(passWord, recvMessage(socket));

	fp=fopen(USER_DATA_FILE,"rb");

	while(!feof(fp)){
		fread(&user,sizeof(user),1,fp);

		if(strcmp(user.name, userName)==0){
			if(strcmp(user.password, passWord)==0)
                found=1;
		}
	}

	fclose(fp);
	
	if(!found){
		sendMessage(socket, "0");
		return 0;
	}else{
		sendMessage(socket, "1");
        strcpy(username, userName);
		return 1;
	}
}

void buatLog(char *perintah){
	time_t rawtime;
	struct tm *timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
 
	char dataLog[LENGTH];

	FILE *file;
	file = fopen(LOG_FILE, "a");

	snprintf(dataLog, sizeof(dataLog)+sizeof(username)+sizeof(perintah), "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s\n",timeinfo->tm_year + 1900, timeinfo->tm_mon + 1, timeinfo->tm_mday, timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec, username, perintah);

	fputs(dataLog, file);
	fclose(file);
	return;
}

char* getAfter(char* find, char* perintah){
    size_t needle_length = strlen(find);
    char const *needle_pos = strstr(perintah, find);
    char *res;
    char word[LENGTH] = "";
    sscanf(needle_pos + needle_length, "%s", word);
    if(strcmp(word, "")== 0) return "";
    res = word;
    return res;
}

int userExist(char *userName){
	FILE *fp;
	struct allowed user;
	int id,found=0;
	fp=fopen(USER_DATA_FILE,"rb");
	while(1){	
		fread(&user,sizeof(user),1,fp);
		if(strcmp(user.name, userName)==0){
			return 1;
		}
		if(feof(fp)){
			break;
		}
	}
	fclose(fp);
	return 0;
	
}

void createUser(char *nama, char *password){
	struct allowed user;
	strcpy(user.name, nama);
	strcpy(user.password, password);
	FILE *fp;
	fp=fopen(USER_DATA_FILE,"ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}

char *getDalamKurung(char *perintah){
    char temp[LENGTH], *res;
    char *ob = strchr(perintah, '(');
    char *cb = strchr(perintah, ')');

    if(ob == NULL) return "";

    sprintf(temp, "%.*s", (int)(cb - ob + 1 ), ob+1);
    res = temp;
    return res;
}

void removeChar(char * str, char charToRemmove){
    int idxToDel;
	for(int i=0; i<strlen(str); i++){
		if(str[i] == charToRemmove){
			idxToDel = i;
			memmove(&str[idxToDel], &str[idxToDel + 1], strlen(str) - idxToDel);
		}
	}
}

int jumlahColumn(char *tableName) {
    int jumlahColumn = 0;
    char filePath[LENGTH], buf[LENGTH];
    snprintf(filePath, sizeof(filePath)+sizeof(databaseUsed)+sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);
    

    FILE *fptr;
    fptr = fopen(filePath, "r");
    if(fptr == NULL) perror("error : open file");

    fseek(fptr, 0, SEEK_END);
    long fsize = ftell(fptr);
    rewind(fptr);

    fread(buf, 1, fsize, fptr);

    for (char *itr = buf; *itr != '\n'; itr++) {
        if (*itr == '\t') jumlahColumn++;
    }

    jumlahColumn++;
    fclose(fptr);
    return jumlahColumn;
    
}

int jumlahRow(char *tableName) {
    int jumlahRow = 0;
    char filePath[LENGTH], buf[LENGTH];
    snprintf(filePath, sizeof(filePath)+sizeof(databaseUsed)+sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);
    

    FILE *fptr;
    fptr = fopen(filePath, "r");
    if(fptr == NULL) perror("error : open file");

    fseek(fptr, 0, SEEK_END);
    long fsize = ftell(fptr);
    rewind(fptr);

    fread(buf, 1, fsize, fptr);

    for (char *itr = buf; *itr != EOF; itr++) {
        if (*itr == '\n') jumlahRow++;
    }

    fclose(fptr);
    return jumlahRow;
    
}

int indexColumn(char *tableName, char* columnName){
    int jumlahData = jumlahRow(tableName);
    int jumlahKolom = jumlahColumn(tableName);
    int index=-1;
    char filePath[LENGTH], buf[LENGTH], temp[LENGTH];
    char data[jumlahData][jumlahKolom][LENGTH];
    snprintf(filePath, sizeof(filePath)+sizeof(databaseUsed)+sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);
    
    FILE *fptr;
    fptr = fopen(filePath, "r");

    if(fptr == NULL) perror("error");

    fgets(buf, LENGTH, fptr);
    for(int j=0; j<jumlahKolom; j++){
        if(j==0) strcpy(data[0][j], getAfter("", buf));
        else strcpy(data[0][j], getAfter(data[0][j-1], buf));
    }

    for(int i=0;i<jumlahKolom;i++){
        strcpy(temp, data[0][i]);
        if(strcmp(columnName, strtok(temp, "|")) == 0)
            index = i;
        bzero(temp, sizeof(temp));
    }
    fclose(fptr);
    return index;
}

int getRowData(char *tableName, char* columnName, char* dataCari){
    int jumlahData = jumlahRow(tableName);
    int jumlahKolom = jumlahColumn(tableName);
    int index=-1, indexKolom = indexColumn(tableName, columnName);
    char filePath[LENGTH], buf[LENGTH];
    char data[jumlahData][jumlahKolom][LENGTH];
    snprintf(filePath, sizeof(filePath)+sizeof(databaseUsed)+sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);
    
    FILE *fptr;
    fptr = fopen(filePath, "r");

    if(fptr == NULL) perror("error");

    for (int i=0; i<jumlahData;i++) {
        fgets(buf, LENGTH, fptr);
        for(int j=0; j<jumlahKolom; j++){
            if(j==0) strcpy(data[i][j], getAfter("", buf));
            else strcpy(data[i][j], getAfter(data[i][j-1], buf));
        }
    }

    for(int i=0;i<jumlahKolom;i++){
        if(strcmp(dataCari, data[i][indexKolom]) == 0)
            index = i;
    }
    fclose(fptr);
    return index;
}


int createColumn(char *perintah){
    char newTable[LENGTH], filePath[LENGTH], temp[LENGTH], *token;
    int jumlahColumn = 0;

    strcpy(temp, getDalamKurung(perintah));

    if(strcmp(temp, "") == 0) return 0;

    for(int i=0; i<strlen(temp);i++){
        if(temp[i] == ',') jumlahColumn++;
    }
    jumlahColumn++;


    column columns[jumlahColumn];

    int j = 0;
    token = strtok(temp, ", ");
    while (token != NULL) {
        sprintf(columns[j].nama, "%s", token);
        token = strtok(NULL, ", ");

        sprintf(columns[j].type, "%s", token);
        token = strtok(NULL, ", ");

        ++j;
    }

    strcpy(newTable, getAfter("TABLE", perintah));
    snprintf(filePath, sizeof(filePath)+sizeof(databaseUsed)+sizeof(newTable), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, newTable);

    FILE *fptr;
    fptr = fopen(filePath, "w");

    for (int i = 0; i < j; i++) {
        removeChar(columns[i].nama, ')');
        removeChar(columns[i].type, ')');
        if (i < j - 1) {
            bzero(temp, sizeof(temp));
            fprintf(fptr, "%s|%s\t", columns[i].nama, columns[i].type);
        } else {
            bzero(temp, sizeof(temp));
            fprintf(fptr, "%s|%s\n", columns[i].nama, columns[i].type);
        }
    }

    fclose(fptr);
    return 1;
}

void insertPermission(char *nama, char *database){
	struct databasePermission user;
	strcpy(user.name, nama);
	strcpy(user.database, database);
	FILE *fp;
	fp=fopen(PERMISSION_FILE,"ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}

int cekDatabasePermission(char *nama, char *database){
	FILE *fp;
	struct databasePermission user;
	int id,found=0;
	fp=fopen(PERMISSION_FILE,"rb");
	while(1){
		fread(&user,sizeof(user),1,fp);
		if(strcmp(user.name, nama)==0){
			if(strcmp(user.database, database)==0){
				return 1;
			}
		}
		if(feof(fp)){
			break;
		}
	}
	fclose(fp);
	return 0;
}

void useCommand(int socket, char* perintah){
    char message[LENGTH], database[LENGTH];
    strcpy(database, getAfter("USE", perintah));

    if(!cekDatabasePermission(username, database) && !root){
        sendMessage(socket, "Permission Denied");
    }else{
        strcpy(databaseUsed, database);
        sprintf(message, "%s used", databaseUsed);
        sendMessage(socket, message);
        buatLog(perintah);
    }
}

void createCommand(int socket, char* perintah){
    if(strstr(perintah, "USER ")){
        if(!root){
            sendMessage(socket, "Permission Denied");
            return;
        }
        char newUsername[LENGTH], newPassword[LENGTH];
        strcpy(newUsername, getAfter("USER", perintah));
        strcpy(newPassword, getAfter("IDENTIFIED BY", perintah));
        createUser(newUsername, newPassword);
        sendMessage(socket, "User Created");
    }else if(strstr(perintah, "DATABASE ")){
        char newDatabase[LENGTH], lokasi[LENGTH];
        strcpy(newDatabase, getAfter("DATABASE", perintah));
        snprintf(lokasi, sizeof(lokasi)+sizeof(newDatabase), "%s/%s", DATABASES_FOLDER, newDatabase);
        insertPermission(username, newDatabase);
        printf("%s", lokasi);
        mkdir(lokasi,0777);
        sendMessage(socket, "Database Created");
    }else if(strstr(perintah, "TABLE ")){

        if(strcmp(databaseUsed, "") == 0){
            sendMessage(socket, "Database didn't selected");
            return;
        }
        if(createColumn(perintah))
            sendMessage(socket, "Table Created");
        else
            sendMessage(socket, "There's no data");
    }else{
        sendMessage(socket, "Incorrect command");
        return;
    }
    buatLog(perintah);
}

void grantCommand(int socket, char* perintah){
    char userDatabase[LENGTH], userName[LENGTH];
    if(!root){
        sendMessage(socket, "Permission Denied");
        return;
    }else{
        strcpy(userDatabase, getAfter("GRANT PERMISSION", perintah));
        strcpy(userName, getAfter("INTO", perintah));

        if(!userExist(userName)){
            sendMessage(socket, "User Not Found");
            return;
        }else{
            insertPermission(userName, userDatabase);
            sendMessage(socket, "Permission given");
        }
    }
    buatLog(perintah);
}

void dropCommand(int socket, char* perintah){
    if(strstr(perintah, "TABLE ")){

        if(strcmp(databaseUsed, "") == 0){
            sendMessage(socket, "Database didn't selected");
            return;
        }
    
        char filePath[LENGTH], tableName[LENGTH];
        strcpy(tableName, getAfter("TABLE", perintah));
        snprintf(filePath, sizeof(filePath)+sizeof(databaseUsed)+sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);
        remove(filePath);
        sendMessage(socket, "Table Has Been Removed");
    }else if(strstr(perintah, "DATABASE ")){
        char databaseName[LENGTH], path[LENGTH];
        strcpy(databaseName, getAfter("DATABASE", perintah));
        snprintf(path, sizeof(path)+sizeof(databaseName), "rm -r %s/%s", DATABASES_FOLDER, databaseName);
        system(path);
        sendMessage(socket, "Database Has Been Removed");
    }else if(strstr(perintah, "DATABASE")){
        if(strcmp(databaseUsed, "") == 0){
            sendMessage(socket, "Database didn't selected");
            return;
        }

        char path[LENGTH];
        snprintf(path, sizeof(path)+sizeof(databaseUsed), "rm -r %s/%s", DATABASES_FOLDER, databaseUsed);
        system(path);
        sendMessage(socket, "Database Has Been Removed");
        strcpy(databaseUsed, "");
    }else if(strstr(perintah, "COLUMN")){
        if(strcmp(databaseUsed, "") == 0){
            sendMessage(socket, "Database didn't selected");
            return;
        }
        char tableName[LENGTH], buf[LENGTH], filePath[LENGTH];
        int indexKolom;

        strcpy(tableName, getAfter("FROM", perintah));
        snprintf(filePath, sizeof(filePath)+sizeof(databaseUsed)+sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);

        int jumlahData = jumlahRow(tableName);
        int jumlahKolom = jumlahColumn(tableName);
        char data[jumlahData][jumlahKolom][LENGTH];

        indexKolom = indexColumn(tableName, getAfter("COLUMN", perintah));

        FILE *fptr, *tempFptr;
        fptr = fopen(filePath, "r");
        tempFptr = fopen("temp", "w");

        if(fptr == NULL) perror("error");

        for (int i=0; i<jumlahData;i++) {
            fgets(buf, LENGTH, fptr);
            for(int j=0; j<jumlahKolom; j++){
                if(j==0) strcpy(data[i][j], getAfter("", buf));
                else strcpy(data[i][j], getAfter(data[i][j-1], buf));
            }
        }

        for(int i=0; i<jumlahData; i++){
            for(int j=0; j<jumlahKolom; j++){
                if(j == indexKolom) continue;

                if (j < (jumlahKolom-1)) {
                    fprintf(tempFptr, "%s\t", data[i][j]);
                } else {
                    fprintf(tempFptr, "%s\n", data[i][j]);
                }
            }
        }
        fclose(fptr);
        fclose(tempFptr);
        remove(filePath);
        rename("temp", filePath);
        sendMessage(socket, "Drop column succesfully");
    }else{
        sendMessage(socket, "Incorrect command");
        return;
    }
    buatLog(perintah);
}

void insertCommand(int socket, char* perintah){
    if(strcmp(databaseUsed, "") == 0){
        sendMessage(socket, "Database didn't selected");
        return;
    }

    char data[LENGTH], tableName[LENGTH], filePath[LENGTH], temp[100][LENGTH], *token;
    strcpy(tableName, getAfter("INTO", perintah));
    strcpy(data, getDalamKurung(perintah));


    snprintf(filePath, sizeof(filePath)+sizeof(databaseUsed)+sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);

    FILE *fptr;
    fptr = fopen(filePath, "a");

    int j = 0;
    token = strtok(data, ", ");
    while (token != NULL) {
        strcpy(temp[j], token);
        token = strtok(NULL, ", ");
        ++j;
    }

    if(j>jumlahColumn(tableName)){
        sendMessage(socket, "Data exceeds the number of columns ");
        bzero(temp, sizeof(temp));
        return;
    }

    for (int i = 0; i < j; i++) {
        removeChar(temp[i], '\'');
        removeChar(temp[i], '`');
        removeChar(temp[i], '"');
        removeChar(temp[i], ')');
        if (i < j - 1) {
            fprintf(fptr, "%s\t", temp[i]);
        } else {
            fprintf(fptr, "%s\n", temp[i]);
        }
    }
    fclose(fptr);
    sendMessage(socket, "Data added succesfully");
    buatLog(perintah);
}

void updateCommand(int socket, char *perintah){
    char tableName[LENGTH], columnName[LENGTH], filePath[LENGTH], buf[LENGTH], dataGanti[LENGTH], tempFile[LENGTH], temp[LENGTH];
    int jumlahData, jumlahKolom, index=-1;

    if(strcmp(databaseUsed, "") == 0){
        sendMessage(socket, "Database didn't selected");
        return;
    }

    strcpy(tableName, getAfter("UPDATE", perintah));
    snprintf(filePath, sizeof(filePath)+sizeof(databaseUsed)+sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);
    
    jumlahData = jumlahRow(tableName);
    jumlahKolom = jumlahColumn(tableName);

    char data[jumlahData][jumlahKolom][LENGTH];

    FILE *fptr, *tempFptr;
    fptr = fopen(filePath, "r");
    tempFptr = fopen("temp", "w");

    if(fptr == NULL) perror("error");

    for (int i=0; i<jumlahData;i++) {
        fgets(buf, LENGTH, fptr);
        for(int j=0; j<jumlahKolom; j++){
            if(j==0) strcpy(data[i][j], getAfter("", buf));
            else strcpy(data[i][j], getAfter(data[i][j-1], buf));
        }
    }

    strcpy(columnName, getAfter("SET", perintah));
    strcpy(columnName, strtok(columnName, "="));

    strcpy(dataGanti, getAfter("=", perintah));
    removeChar(dataGanti, '\'');
    removeChar(dataGanti, '"');

    for(int i=0;i<jumlahKolom;i++){
        strcpy(temp, data[0][i]);
        if(strcmp(columnName, strtok(temp, "|")) == 0)
            index = i;
        bzero(temp, sizeof(temp));
    }
    
    if(index == -1){
        sendMessage(socket, "There's no Column with that name");
        return;
    }

    if(strstr(perintah, "WHERE")){
        char columnName[LENGTH], *token, value[LENGTH];
        strcpy(columnName, getAfter("WHERE", perintah));
        token = strtok(columnName, "=");
        token = strtok(NULL, "=");
        strcpy(value, token);

        int barisGanti = getRowData(tableName, columnName, value);
        if(barisGanti == -1){
            sendMessage(socket, "There's no such value in that column");
            return;
        }
        strcpy(data[barisGanti][index], dataGanti);
    }else{
        for(int i=1; i<jumlahData; i++){
            strcpy(data[i][index], dataGanti);
        }
    }

    for(int i=0; i<jumlahData; i++){
        for(int j=0; j<jumlahKolom; j++){
            if (j < (jumlahKolom-1)) {
                fprintf(tempFptr, "%s\t", data[i][j]);
            } else {
                fprintf(tempFptr, "%s\n", data[i][j]);
            }
        }
    }

    fclose(fptr);
    fclose(tempFptr);
    remove(filePath);
    rename("temp", filePath);
    sendMessage(socket, "Update succesfully");
    buatLog(perintah);
}

void deleteCommand(int socket, char* perintah){
    char tableName[LENGTH], filePath[LENGTH], buf[LENGTH], tempFile[LENGTH], temp[LENGTH];
    int jumlahData, jumlahKolom, index=-1;

    if(strcmp(databaseUsed, "") == 0){
        sendMessage(socket, "Database didn't selected");
        return;
    }

    strcpy(tableName, getAfter("DELETE FROM", perintah));
    snprintf(filePath, sizeof(filePath)+sizeof(databaseUsed)+sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);
    
    jumlahData = jumlahRow(tableName);
    jumlahKolom = jumlahColumn(tableName);

    char data[jumlahData][jumlahKolom][LENGTH];

    FILE *fptr, *tempFptr;
    fptr = fopen(filePath, "r");
    tempFptr = fopen("temp", "w");

    if(fptr == NULL) perror("error");

    for (int i=0; i<jumlahData;i++) {
        fgets(buf, LENGTH, fptr);
        for(int j=0; j<jumlahKolom; j++){
            if(j==0) strcpy(data[i][j], getAfter("", buf));
            else strcpy(data[i][j], getAfter(data[i][j-1], buf));
        }
    }

    if(strstr(perintah, "WHERE")){
        char columnName[LENGTH], *token, value[LENGTH];
        strcpy(columnName, getAfter("WHERE", perintah));
        token = strtok(columnName, "=");
        token = strtok(NULL, "=");
        strcpy(value, token);

        for(int i=0;i<jumlahKolom;i++){
            strcpy(temp, data[0][i]);
            if(strcmp(columnName, strtok(temp, "|")) == 0)
                index = i;
            bzero(temp, sizeof(temp));
        }

        if(index == -1){
            sendMessage(socket, "There's no Column with that name");
            remove("temp");
            return;
        }

        int barisGanti = getRowData(tableName, columnName, value);
        if(barisGanti == -1){
            sendMessage(socket, "There's no such value in that column");
            remove("temp");
            return;
        }
        strcpy(data[barisGanti][index], "\t");

        for(int i=0; i<jumlahData; i++){
            for(int j=0; j<jumlahKolom; j++){
                if (j < (jumlahKolom-1)) {
                    fprintf(tempFptr, "%s\t", data[i][j]);
                } else {
                    fprintf(tempFptr, "%s\n", data[i][j]);
                }
            }
        }
    }else{
        for(int i=0; i<jumlahKolom; i++){
            if (i < jumlahKolom - 1) {
                fprintf(tempFptr, "%s\t", data[0][i]);
            } else {
                fprintf(tempFptr, "%s\n", data[0][i]);
            }
        }
    }

    fclose(fptr);
    fclose(tempFptr);

    remove(filePath);
    rename("temp", filePath);
    sendMessage(socket, "Delete succesfully");
    buatLog(perintah);
}

void selectCommand(int socket, char* perintah){
    char tableName[LENGTH], filePath[LENGTH], message[LENGTH]="", buf[LENGTH], temp[LENGTH], *token;
    if(strcmp(databaseUsed, "") == 0){
        sendMessage(socket, "Database didn't selected");
        return;
    }
    
    strcpy(tableName, getAfter("FROM", perintah));
    snprintf(filePath, sizeof(filePath)+sizeof(databaseUsed)+sizeof(tableName), "%s/%s/%s", DATABASES_FOLDER, databaseUsed, tableName);

    int jumlahData = jumlahRow(tableName);
    int jumlahKolom = jumlahColumn(tableName);

    char data[jumlahData][jumlahKolom][LENGTH];

    FILE *fptr;
    fptr = fopen(filePath, "r");

    if(fptr == NULL) perror("error");

    for (int i=0; i<jumlahData;i++) {
        fgets(buf, LENGTH, fptr);
        for(int j=0; j<jumlahKolom; j++){
            if(j==0) strcpy(data[i][j], getAfter("", buf));
            else strcpy(data[i][j], getAfter(data[i][j-1], buf));
        }
    }

    if(strcmp(getAfter("SELECT", perintah), "*")==0){
        for(int i=0; i<jumlahData; i++){
            for(int j=0; j<jumlahKolom; j++){
                if( i == 0) strtok(data[i][j], "|");

                if (j < jumlahKolom - 1) {
                    strcat(message, data[i][j]);
                    strcat(message, "\t");
                } else {
                    strcat(message, data[i][j]);
                    strcat(message, "\n");
                }
            }
        }
    }else{
        int jumlahSelectedColumn=0, indexSelectedColumn[100];
        char selectedColumn[100][LENGTH];
        
        strcpy(temp, perintah);
        token = strtok(temp, ", ");
        token = strtok(NULL, ", ");
        while (token != NULL) {
            if(strcmp(token, "FROM") == 0) break;
            strcpy(selectedColumn[jumlahSelectedColumn], token);
            jumlahSelectedColumn++;
            token = strtok(NULL, ", ");
        }
        
        for(int j=0; j<jumlahSelectedColumn; j++)
                indexSelectedColumn[j] = indexColumn(tableName, selectedColumn[j]);
        
        for(int i=0; i<jumlahData; i++){
            for(int j=0; j<jumlahSelectedColumn; j++){
                if( i == 0) strtok(data[i][indexSelectedColumn[j]], "|");

                if (j < jumlahSelectedColumn - 1) {
                    strcat(message, data[i][indexSelectedColumn[j]]);
                    strcat(message, "\t");
                } else {
                    strcat(message, data[i][indexSelectedColumn[j]]);
                    strcat(message, "\n");
                }
            }
        }
    }
    sendMessage(socket, message);

    fclose(fptr);
    buatLog(perintah);
}

void dumpCommand(int socket, char* perintah){
    char databaseName[LENGTH], filePath[LENGTH], data[LENGTH][LENGTH], buf[LENGTH], message[LENGTH]="", *token;
    int jumlahData = 0;
    strcpy(databaseName, getAfter("DUMP", perintah));

    FILE *fptr;
    fptr = fopen(LOG_FILE, "r");

    if(fptr == NULL) perror("error");

    fseek(fptr, 0, SEEK_END);
    long fsize = ftell(fptr);
    rewind(fptr);

    fread(buf, 1, fsize, fptr);

    for (char *itr = buf; *itr != EOF; itr++) {
        if (*itr == '\n') jumlahData++;
    }

    bzero(buf, sizeof(buf));
    fclose(fptr);

    int sesuai=0;
    fptr = fopen(LOG_FILE, "r");
    for (int i=0; i<jumlahData;i++) {
        fgets(buf, LENGTH, fptr);
        token = strtok(buf, ":");
        token = strtok(NULL, ":");
        token = strtok(NULL, ":");
        token = strtok(NULL, ":");
        token = strtok(NULL, ":");
        if(strstr(token, "USE") || strstr(token, "CREATE")){
            if(strstr(token, databaseName)) sesuai = 1;
            else sesuai = 0;
        }else{
            if(sesuai) strcat(message, token);
        }
    }
    fclose(fptr);
    sendMessage(socket, message);
}

int getPerintah(int socket){
    char perintah[LENGTH];
    strcpy(perintah, recvMessage(socket));

    if(strstr(perintah, "CREATE ")) createCommand(socket, perintah);
    else if(strstr(perintah, "USE")) useCommand(socket, perintah);
    else if(strstr(perintah, "GRANT PERMISSION")) grantCommand(socket, perintah);
    else if(strstr(perintah, "DROP")) dropCommand(socket, perintah);
    else if(strstr(perintah, "INSERT")) insertCommand(socket, perintah);
    else if(strstr(perintah, "UPDATE")) updateCommand(socket, perintah);
    else if(strstr(perintah, "DELETE FROM")) deleteCommand(socket, perintah);
    else if(strstr(perintah, "SELECT")) selectCommand(socket, perintah);
    else if(strstr(perintah, "DUMP")) dumpCommand(socket, perintah);
    else if(strstr(perintah, "EXIT")) return 0;
    else sendMessage(socket, "Command not found");
    
    return 1;
}

int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int server_fd, new_socket, opt = 1, addrlen = sizeof(address);
    char buffer[LENGTH] = {0};

	// =============== KONEKSI =================
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) {
        perror("socket failed");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, &opt, sizeof(opt))) {
        perror("setsockopt");
        exit(EXIT_FAILURE);
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = INADDR_ANY;
    address.sin_port = htons( PORT );

    if (bind(server_fd, (struct sockaddr *)&address, sizeof(address))<0) {
        perror("bind failed");
        exit(EXIT_FAILURE);
    }

    if (listen(server_fd, 3) < 0) {
        perror("listen");
        exit(EXIT_FAILURE);
    }

    if ((new_socket = accept(server_fd, (struct sockaddr *)&address, (socklen_t*)&addrlen))<0) {
        perror("accept");
        exit(EXIT_FAILURE);
    }
	// ============= END KONEKSI ===============

    root = cekRoot(new_socket);

    if(auth(new_socket))
        while(getPerintah(new_socket));
    
    return 0;
}