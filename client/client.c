#include <stdio.h>
#include <sys/socket.h>
#include <stdlib.h>
#include <netinet/in.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>

#define PORT 8080
#define LENGTH 1024

char buffer[1024] = {0};
char message[LENGTH];

struct allowed{
	char name[LENGTH];
	char password[LENGTH];
};

char *recvMessage(int socket){
	int temp;
	temp = read(socket , buffer, LENGTH);
	if(!temp){
		printf("Terdapat kesalahan dalam penerimaan pesan\n");
		exit(EXIT_FAILURE);
	}
    return buffer;
}

char *sendMessage(int socket, char* message){
	int temp;
	temp = send(socket , message , sizeof(buffer) , 0 );
	if(!temp){
		printf("Terdapat kesalahan dalam pengiriman pesan\n");
		exit(EXIT_FAILURE);
	}
}

int cekData(int socket, const char *username, const char *password){
	char userName[LENGTH], passWord[LENGTH];
	strcpy(userName, username);
	strcpy(passWord, password);
	sendMessage(socket, userName);
	sendMessage(socket, passWord);

	if(atoi(recvMessage(socket)) == 0){
		printf("You're not allowed");
		return 0;
	}

	return 1;
}

void removeChar(char * str, char charToRemmove){
    int idxToDel;
	for(int i=0; i<strlen(str); i++){
		if(str[i] == charToRemmove){
			idxToDel = i;
			memmove(&str[idxToDel], &str[idxToDel + 1], strlen(str) - idxToDel);
			break;
		}
	}
}

int getPerintah(int socket){
	char input[LENGTH];
	scanf(" %[^\n]s", input);
	removeChar(input, ';');

	if(strcmp(input, "EXIT") == 0){
		sendMessage(socket, input);
		return 0;
	}

	sendMessage(socket, input);
	return 1;
}

int main(int argc, char const *argv[]) {
    struct sockaddr_in address;
    int sock = 0, allowed = 0;
    struct sockaddr_in serv_addr;

	// =============== KONEKSI KE SERVER =================
	if ((sock = socket(AF_INET, SOCK_STREAM, 0)) < 0) {
        printf("\n Socket creation error \n");
        return -1;
    }

    memset(&serv_addr, '0', sizeof(serv_addr));

    serv_addr.sin_family = AF_INET;
    serv_addr.sin_port = htons(PORT);

    if(inet_pton(AF_INET, "127.0.0.1", &serv_addr.sin_addr)<=0) {
        printf("\nInvalid address/ Address not supported \n");
        return -1;
    }

    if (connect(sock, (struct sockaddr *)&serv_addr, sizeof(serv_addr)) < 0) {
        printf("\nConnection Failed \n");
        return -1;
    }
	// ============= END KONEKSI KE SERVER ===============

	// ================== SEND Data ===========================
	sprintf(message, "%d", geteuid());
	sendMessage(sock, message);

	if(geteuid() == 0) allowed=1;
	else allowed = cekData(sock, argv[2],argv[4]);

	if(!allowed) return 0;
	// ================ END SEND Data =========================
	if(strcmp(argv[6], "SHELL=/bin/bash") != 0){
		char buf[LENGTH]="", temp[LENGTH];
		int jumlahData=0;
		fseek(stdin, 0, SEEK_END);
		long fsize = ftell(stdin);
		rewind(stdin);

		fread(buf, 1, fsize, stdin);

		FILE *fptr;
		fptr = fopen("tamp", "w");

		fprintf(fptr, "%s", buf);
		fclose(fptr);

		for (char *itr = buf; *itr != EOF; itr++) {
			if (*itr == '\n'){
				jumlahData++;
			}
		}

		sprintf(message, "USE %s", argv[6]);
		sendMessage(sock, message);
		printf("%s\n", recvMessage(sock));

		fptr = fopen("tamp", "r");

		for (int i=0; i<jumlahData;i++) {
			fgets(temp, LENGTH, fptr);
			strtok(temp, "\n");
			sendMessage(sock, temp);
			printf("%s\n", recvMessage(sock));
		}
		fclose(fptr);
		remove("tamp");
		return 0;
	}

	while(getPerintah(sock)){
		printf("%s\n", recvMessage(sock));
	}

    return 0;
}